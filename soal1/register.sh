#!/bin/bash

tgl=$(date +%D)
jam=$(date +%T)
echo "Username"
read uservar
echo "Password"
read -s passvar
len="${#passvar}"
min=8
if grep -q  $uservar ./users/user.txt
then
        echo "User already exists"
        echo $tgl $jam REGISTER: ERROR User already exists >> ./log.txt
        exit 0
elif [ "$uservar" = "$passvar" ]
then
        echo "Password can't be the same as username"
        exit 0
elif [ $len -lt $min ]
then
        echo "Password is at least eight characters"
        exit 0
fi

if [[ "$passvar" =~  [A-Z] ]]
then
        flag=1
else
        echo "Minimal 1 upper-case letters"
        exit 0
fi

if [[ "$passvar" =~  [a-z] ]]
then
        flag=2
else
        echo "Minimal 1 lower-case letters"
        exit 0
fi

if [[ "$passvar" =~ [0-9] ]]
then

        echo $uservar "registered successfully"
        echo $tgl $jam $uservar $passvar >> ./users/user.txt
   echo $tgl $jam REGISTER:INFO User $uservar registered succesfully >> ./log.txt
else
        echo "Minimal 1 digit number"
        exit 0
fi
