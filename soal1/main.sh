#!/bin/bash
loginfunc(){
        if grep -q -w "$uservar $passvar" ./users/user.txt
        then
                echo "$tgl $jam LOGIN:INFO User $uservar logged in" >> ./log.txt
                echo "Login successfully"
                echo "pilih command"
                read perintah
                if [ "$perintah" = "$downvar" ]
                then
                        downfunc
                elif [ "$perintah" = "$checkvar" ]
                then
                        checkfunc
                else
                        echo "Command not valid"
                fi
        else
                echo $tgl $jam LOGIN:ERROR Failed login attemp on user User $uservar >> ./log.txt
                echo -e "Failed login\nWrong username or password"
        fi
                }
checkfunc(){
        awk -v user="$uservar" 'BEGIN {count=0} $5 == user || $9 == user {count++} END {print (count)}' log.txt
}
downfunc(){
        read n
        namafile=$(date +%y-%m-%d)_$uservar
        if [[ ! -f "$namafile.zip" ]]
        then
                mkdir $namafile
                for(( i=$banyak+1; i<=$n+$banyak; i++ ))
                do
                        wget https://loremflickr.com/320/240 -O $namafile/PIC_$i.jpg
                done
                zip --password $passvar -r $namafile.zip $namafile
                rm -rf $namafile
        else
                unzip -P $passvar $namafile.zip
                rm $namafile.zip
                banyak=$(find $namafile -type f | wc -l)
                for(( i=$banyak+1; i<=$n+$banyak; i++ ))
                do
                        wget https://loremflickr.com/320/240 -O $namafile/PIC_$i.jpg
                done
                zip --password $passvar -r $namafile.zip $namafile
                rm -rf $namafile
        fi
                }
tgl=$(date +%D)
jam=$(date +%T)
echo "Username"
read uservar
echo "Password"
read -s passvar
downvar="dl"
checkvar="att"
len="${#passvar}"
min=8
banyak=0
if [ "$uservar" = "$passvar" ]
then
        echo "Password can't be the same as username"
        exit 0
elif [ $len -lt $min ]
then
        echo "Password is at least eight characters"
        exit 0
fi

if [[ "$passvar" =~  [A-Z] ]]
then
        flag=1
else
        echo "Minimal 1 upper-case letters"
        exit 0
fi

if [[ "$passvar" =~  [a-z] ]]
then
        flag=2
else
        echo "Minimal 1 lower-case letters"
        exit 0
fi

if [[ "$passvar" =~ [0-9] ]]
then
    loginfunc
else
        echo "Minimal 1 digit number"
        exit 0
fi
