# soal-shift-sisop-modul-1-F06-2022

Ridzki Raihan Alfaza 5025201178  
Muhammad Afif Dwi Ardhiansyah 5025201212  
Antonio Taifan Montana 5025201219

---

# SOAL 1
Tujuan utama dari soal 1 yaitu untuk membuat sistem register dan login. Setiap kegiatan login atau register
akan ditampilkan pada log sesuai dengan status dari kegiatan. Ketika berhasil login, user memiliki dua pilihan,
yaitu att atau dl. Pilihan att akan menampilkan berapa banyak user tersebut telah mencoba untuk login
sejak awal hingga user berhasil login. Pilihan dl akan mendownload image sebanyak N kali, kemudian image tersebut disimpan
di dalam zip.

## Register 
Setiap user yang berhasil register akan disimpan pada /users/user.txt . 
User akan menginputkan username dan password. Terdapat beberapa persyaratan untuk pembuatan password, yaitu:
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumerical
- Tidak boleh sama dengan username

Menggunakan -s pada read untuk membaca input secara hidden
```
echo "Username"
read uservar
echo "Password"
read -s passvar
```

- Langkah selanjutnya kita mengecek apakah username tersebut sudah pernah diinputkan atau belum dengan if grep dimana grep akan mencari pada file sesuai dengan patern yang diinput dengan file di user.txt
```
if grep -q  $uservar ./users/user.txt
then
        echo "User already exists"
        echo $tgl $jam REGISTER: ERROR User already exists >> ./log.txt
        exit 0
```

- Jika username sudah pernah diinputkan cetak tanggal dan waktu REGISTER: ERROR User already exists pada log.txt. Jika belum maka langkah selanjutnya kita mengecek apakah username sama dengan password. Mengecek kalau password tidak sama dengan username dengan mengecek if password -eq username, jika iya maka keluarkan error message, jika tidak maka lanjut untuk syarat selanjutnya

```
elif [ "$uservar" = "$passvar" ]
then
        echo "Password can't be the same as username"
        exit 0
```

- Mengecek minimal 8 karakter dengan mengecek apakah panjang karakter lebih sedikit dari (-lt) 8 atau tidak. Jika iya, maka proses dihentikan dan mengeluarkan message bahwa password minimal 8 karakter, jika tidak lebih sedikit, maka lanjutkan untuk syarat selanjutnya.

```
min=8
len="${#passvar}"
elif [ $len -lt $min ]
then
        echo "Password is at least eight characters"
        exit 0
fi

```
- Mengecek setidaknya terdapat 1 huruf besar, 1 huruf kecil, dan 1 angka. Jika tidak terdapat, maka proses dihentikan dan mengeluarkan message bahwa password minimal 8 karakter, Jika semua kondisi terpenuhi maka akun akan teregistrasi dan Username dan Password akan tersimpan pada ./users/user.txt dan print pesan log di log.txt dengan format MM/DD/YY HH:MM:SS  REGISTER:
INFO User **USERNAME** registered successfully.

```
if [[ "$passvar" =~  [A-Z] ]]
then
        flag=1
else
        echo "Minimal 1 upper-case letters"
        exit 0
fi

if [[ "$passvar" =~  [a-z] ]]
then
        flag=2
else
        echo "Minimal 1 lower-case letters"
        exit 0
fi

if [[ "$passvar" =~ [0-9] ]]
then

        echo $uservar "registered successfully"
        echo $tgl $jam $uservar $passvar >> ./users/user.txt
   echo $tgl $jam REGISTER:INFO User $uservar registered succesfully >> ./log.txt
else
        echo "Minimal 1 digit number"
        exit 0
fi
```
## Login

Sistem login memiliki persyaratan yang sama untuk password seperti register
Menggunakan -s pada read untuk membaca input secara hidden

```
echo "Username"
read uservar
echo "Password"
read -s passvar
```
Dan pengecekan yang sama seperti pada main namun berbeda jika semua kondisi terpenuhi maka akan masuk pada fungsi login
```
if [ "$uservar" = "$passvar" ]
then
        echo "Password can't be the same as username"
        exit 0
elif [ $len -lt $min ]
then
        echo "Password is at least eight characters"
        exit 0
fi

if [[ "$passvar" =~  [A-Z] ]]
then
        flag=1
else
        echo "Minimal 1 upper-case letters"
        exit 0
fi

if [[ "$passvar" =~  [a-z] ]]
then
        flag=2
else
        echo "Minimal 1 lower-case letters"
        exit 0
fi

if [[ "$passvar" =~ [0-9] ]]
then
    loginfunc
else
        echo "Minimal 1 digit number"
        exit 0
fi
```
Pada loginfunc() berisi pengecekan akun pada user.txt dan penentuan langkah selanjutnya. Jika ditemukan username dan password yang sesuai, maka program akan memberikan dua pilihan, yaitu att atau dl N. Jika tidak maka kirim pesan log ke log.txt dengan format "tanggal jam LOGIN:ERROR Failed login attemp on user User **username**"
```
loginfunc(){
        if grep -q -w "$uservar $passvar" ./users/user.txt
        then
                echo "$tgl $jam LOGIN:INFO User $uservar logged in" >> ./log.txt
                echo "Login successfully"
                echo "pilih command"
                read perintah
                if [ "$perintah" = "$downvar" ]
                then
                        downfunc
                elif [ "$perintah" = "$checkvar" ]
                then
                        checkfunc
                else
                        echo "Command not valid"
                fi
        else
                echo $tgl $jam LOGIN:ERROR Failed login attemp on user User $uservar >> ./log.txt
                echo -e "Failed login\nWrong username or password"
        fi
                }

```
Ketika memilih att, maka program menjalankan fungsi att, yaitu untuk mengetahui log info dari username tersebut. Log info yang diinginkan yaitu login attempt dari username tersebut. Login attempt username tersebut dapat ditemukan pada argumen ke 5 atau 9 pada log.txt. Banyaknya attempt dikurangi 1, karena ketika mengecek argumen 5 atau 9, maka log register akan terhitung, karena register hanya sekali, maka banyak attempt dikurangi 1.
```
checkfunc(){
        awk -v user="$uservar" 'BEGIN {count=0} $5 == user || $9 == user {count++} END {print (count-1)}' log.txt
}
```
Pilihan dl merupakan perintah untuk mendownload foto pada website sebanyak N kali. Foto yang di download disimpan dalam sebuah .zip dan diberi password sesuai dengan akun yang sedang mengakses saat itu. Penamaan .zip berdasarkan tanggal, apabila terdapat nama .zip yang sama, maka .zip akan dibuka, kemudian foto yang baru didownload akan digabungkan ke dalam .zip yang lama. Oleh sebab itu, kita perlu mengecek apakah sudah ada file .zip tersebut, dan melakukan langkah selanjutnya berdasarkan hasil pengecekan. Jika sudah ada, maka akan di unzip, kemudian baru mendownload, jika belum ada, maka akan langsung mendownload foto.

```
downfunc(){
        read n
        namafile=$(date +%y-%m-%d)_$uservar
        if [[ ! -f "$namafile.zip" ]]
        then
                mkdir $namafile
                for(( i=$banyak+1; i<=$n+$banyak; i++ ))
                do
                        wget https://loremflickr.com/320/240 -O $namafile/PIC_$i.jpg
                done
                zip --password $passvar -r $namafile.zip $namafile
                rm -rf $namafile
        else
                unzip -P $passvar $namafile.zip
                rm $namafile.zip
                banyak=$(find $namafile -type f | wc -l)
                for(( i=$banyak+1; i<=$n+$banyak; i++ ))
                do
                        wget https://loremflickr.com/320/240 -O $namafile/PIC_$i.jpg
                done
                zip --password $passvar -r $namafile.zip $namafile
                rm -rf $namafile
        fi
                }
```
## Dokumentasi Soal 1

Registrasi

<img src=soal1/gambar/Screenshot(63).png>

File user.txt setelah registrasi akun

<img src=soal1/gambar/Screenshot(64).png>

Ketika registrasi akun yang telah di registrasi sebelumnya

<img src=soal1/gambar/Screenshot(65).png>

Ketiga registrasi dengan password yang tidak mengandung huruf besar

<img src=soal1/gambar/Screenshot(66).png>

Login berhasil

<img src=soal1/gambar/Screenshot(67).png>

File log.txt

<img src=soal1/gambar/Screenshot(71).png>

Memilih dl dengan N=5

<img src=soal1/gambar/Screenshot(68).png>

File .zip

<img src=soal1/gambar/Screenshot(69).png>

Memilih att

<img src=soal1/gambar/Screenshot(70).png>
---

# SOAL 2

## Membuat folder forensic_log_website_daffainfo_log.

Pertama yang akan dilakukan adalah membuat folder baru. Perintah membuat folder baru adalah mkdir. Diberikan -p agar folder tidak terbuat lagii jika folder yang dimaksud sudah ada. Sehingga perintah sebagai berikut:

```
mkdir -p forensic_log_website_daffainfo_log
```

## Rata-rata serangan per jam

Ditanyakan rata-rata serangan per jam. Dilakukan dengan awk mencari tanggal 22/Jan/2022. Pencarian tersebut dihitung dengan cara menambahkan variabel n sejumlah 1 jika tanggal tersebut ditemukan. 

````
awk '/22\/Jan\/2022/ {++n}
````

Selagi menemukan tanggal tersebut, dilakukan cek untuk jam nya apakah masih di jam yang sama. Untuk menemukan jam, dilakukan pemisahan kolom dengan “:”, untuk itu di BEGIN ditulis FS=”:”. Jam berada di kolom ke-3 sehingga dicek “if (jam!=$3). Jika benar maka variabel jml akan ditambah 1, dan jam = $3. 

````
if(jam!=$3){
    jml++
    jam=$3
}
````

Variabel n ini akan kelebihan 1 karena jam adalah jarak/rentang, sehingga n dikurangi 1. Untuk itu dilakukan print sebagai berikut,

````
END {print "Rata-rata serangan adalah sebanyak ",n/(jml-1), "request per jam."}
````

Hasil rata-rata tersebut dimasukkan ke file text bernama ratarata.txt dengan disimpan di folder yang dibuat sebelumnya.

````
' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt
````

## Menampilkan IP serangan terbanyak

### IP dengan serangan terbanyak
Ditanyakan banyak ip yang menyerang server. Dikarenakan log website daffa tidak dipisahkan oleh spasi, dan dipisahkan dengan titik dua “:”, maka harus diidentifikasi dahulu bahwa elemen dipisahkan oleh titik dua, bukan spasi. Maka dituliskan perintah sebagai berikut,

````
ip_terbanyak_ans="$(awk 'BEGIN { FS=":" } 
````

Di atas terdapat variabel “banyak_serangan_ans”, variabel tersebut untuk menyimpan print dari perintah awk ini.

Selanjutnya, baris pertama tidak diikutkan dalam proses awk, maka baris pertama akan di skip dengan cara menuliskan sebagai berikut.

````
NR!=1 { arr[$1]++ }
````

arr[$1]++ adalah untuk menghitung banyak berapa kali ip yang sama menyerang server. Ip ditemukan dengan $1 karena ip berada di kolom pertama. Ip yang ditemukan dijadikan alamat array, dimana nilai dari alamat array tersebut ditambahkan 1 jika ip yang sama ditemukan kembali.

Selanjutnya, jika telah meng-iterasi pada semua baris, maka dilakuakan perintah sebagai berikut,

````
END {
max=0
ip_max=0
for (a in arr){
	if(max < arr[a]){
		max=arr[a]
		ip_max=a
	}
}
print max}
' log_website_daffainfo.log )"
````

Perintah tersebut adalah dengan looping array arr. Variabel a adalah alamat array yaitu ip. Variabel max untuk menyimpan jumlah ip yang melakukan serangan terbanyak. Jika max kurang dari jumlah serangan ip, maka max adalah jumlah serangan ip. Lalu variabel “ip_max” di-print dan print tersebut disimpan di variabel “ip_terbanyak_ans”

### Banyaknya serangan dari serangan terbanyak

Selanjutnya, ditanyakan banyaknya serangan terbanyak. Sebenarnya jumlah serangan terbanyak ini sudah dihitung di perintah di atas. Untuk itu dilakukan perintah di atas lagi, namun yang di-print adalah variabel “max”. Print ini disimpan di variabel “banyak_serangan_max”. Untuk itu perintah sebagai berikut,

````
banyak_serangan_ans="$(awk 'BEGIN { FS=":" }
NR!=1 { arr[$1]++ }
END {
max=0
ip_max=0
for (a in arr){
	if(max < arr[a]){
		max=arr[a]
	}
}
print max}
' log_website_daffainfo.log )"
````

## Banyak serangan user-agent curl

Ditanyakan berapa banyak serangan yang menggunakan user-agent curl. Untuk itu perlu melakukan pencarian “curl” di tiap barisnya. Dilakukan perintah sebagai berikut,

````
curl_ans=$(awk '/curl/ {++n}
````

Variabel “curl_ans” di atas adalah untuk menyimpan print nanti. Perintah ++n untuk menghitung banyaknya “curl”, dan jumlahnya disimpan di variabel “n”.

Selanjutnya, variabel “n” di-print dan disimpan di variabel “curl_ans”. Dilakukan perintah sebagai berikut,

````
END {print n}' log_website_daffainfo.log )
````

## Menampilkan IP yang menyerang server pada jam 2 pagi

Diminta untuk menampilkan ip yang menyerang server pada jam 2 pagi pada tanggal 22/Jan/2022. 

Sebelumnya diperlukan untuk melakukan print untuk tugas-tugas sebelumnya. Print ini dilakukan di awk terakhir karena ketiga tugas awk ini di-print di file text yang sama yaitu result.txt. Untuk itu perlu menangkap variabel-variabel sebelumnya ke dalam awk ini dengan melakukan perintah -v. Perintah sebagai berikut,

````
awk -v curl_ans=$curl_ans -v ip_terbanyak_ans=$ip_terbanyak_ans -v banyak_serangan_ans=$banyak_serangan_ans '
````

Karena tugas ketiga ini yang ditanyakan hanya ip, maka perlu pemisahan dengan “:”, dilakukan sebagai berikut,

````
BEGIN { FS=":"
````

Tugas-tugas sebelumnya perlu di-print, maka dilakukan sebagai berikut,

````
print "IP yang paling banyak mengakses serveradalah:", ip_terbanyak_ans, "sebanyak", banyak_serangan_ans, "request\n"

printf("Ada %d request yang menggunakan curl sebagai user-agent\n\n", curl_ans)}
````

Kembali ke tugas ketiga, ditanyakan ip mana saja yang melakukan penyerangan pada jam 2 pagi tanggal 22/Jan/2022. Dilakukan pencarian pada tiap barisnya yaitu “22/Jan/2022:02”. Jika ditemukan maka akan di-print pada kolom pertama dengan $1. Maka dilakukan perintah sebagai berikut,

````
/22\/Jan\/2022:02/ {print $1}
````

Hasil print ketiga tugas ini di-print pada file txt bernama result.txt pada folder forensic_log_website_daffainfo_log. Sehingga ditulis sebagai berikut,

````
' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt
````

---

# SOAL 3

## minute_log.sh

untuk mendapat data penggunaan ram, output dari command `free -m` dijadikan sebuah array.

```bash
    ram_usage=($(free -m))
```

dari array `ram_usage`, diambil variabel yang diminta sesuai dengan urut muncul variabel tersebut pada array.

```bash
    mem_total=${ram_usage[7]}
    mem_used=${ram_usage[8]}
    mem_free=${ram_usage[9]}
    mem_shared=${ram_usage[10]}
    mem_buff=${ram_usage[11]}
    mem_available=${ram_usage[12]}
    swap_total=${ram_usage[14]}
    swap_used=${ram_usage[15]}
    swap_free=${ram_usage[16]}
```

metode yang diatas dipakai juga untuk mendapatkan disk usage, karena soal meminta disk usage pada home directory, target path dapat dideklarasi sebagai `~`

```bash
    target_path=~           #/home/usr
    disk_usage=($(du -sh $target_path))
    path_size=${disk_usage[0]}
```

membuat file log baru sesuai dengan format penamaan, detail waktu didapatkan dari command `date`, dan file berisi variabel yang sudah didapatkan.

```bash
    log_path=~/log/metrics_$(date +'%y%m%d%H%M%S').log
    touch $log_path
    echo -e "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,$target_path,$path_size" > $log_path
```

mengatur permission penuh kepada user dan tidak memberi permission kepada user lain atau group user dengan command `chmod`

```bash
    chmod 700 $log_path
```

## aggregate_minutes_to_hourly_log.sh

mencari semua file yang dibuat 1 jam sebelum script dirun

```bash
cd ~/log/
current_hour=$(date +'%H');
current_hour=$((current_hour - 1))
log_paths_substring=metrics_$(date +'%y%m%d')$current_hour
log_files=$(find -maxdepth 1 -name "$log_paths_substring*")  #find all files from the last hour
```

inisialisasi array hasil agregasi

```bash
maxInt=1000000

log_totals=(0 0 0 0 0 0 0 0 0 0) #empty array
log_avg=(0 0 0 0 0 0 0 0 0 0)
log_min=($maxInt $maxInt $maxInt $maxInt $maxInt $maxInt $maxInt $maxInt $maxInt $maxInt)
log_max=(0 0 0 0 0 0 0 0 0 0)
total_logs=0
```

iterasi tiap file log dari 1 jam yang lalu

```bash
for log_file in $log_files
do
```

increment jumlah file log yang ada

```bash
    total_logs=$((total_logs + 1))
```

membaca log file dan mengambil nilai dari log file

```bash
    exec < $log_file
    read header
    read values
```

karena file log dalam format csv, format seperator diganti menjadi tanda koma agar input dapat menjadi bentuk array

```bash
    IFS=,                       #change to accept commas as a seperator
    values_array=($values)
```

mengubah disk usage dari format human readable menjadi byte menggunakan command numfmt

```bash
    values_array[9]=$(numfmt --from=iec ${values_array[10]}) #convert human readable to bytes
```

looping per value untuk mendapat minimal, maksimal, dan total per variabel.

```bash
    for i in {0..9}
    do
        log_totals[$i]=$((log_totals[$i]+values_array[$i]))
        if [ $((values_array[$i])) -gt $((log_max[$i])) ]
        then
            log_max[$i]=$((values_array[$i]))
        fi
        if [ $((values_array[$i])) -lt $((log_min[$i])) ]
        then
            log_min[$i]=$((values_array[$i]))
        fi
    done
```

mengembalikan format seperator kembali menjadi endline sebelum loop selanjutnya

```bash
    IFS="\n"
done
```

menghitung rata-rata tiap value

```bash
j=0
for value in ${log_totals[@]}
do
    log_avg[$j]=$((value/total_logs))
    j=$((j+1))
done
```

mengembalikan disk usage menjadi format human readable.

```bash
log_min[9]=$(numfmt --to=iec ${log_min[9]}) #convert back into human readable format
log_max[9]=$(numfmt --to=iec ${log_max[9]})
log_avg[9]=$(numfmt --to=iec ${log_avg[9]})
```

membuat file log agregasi dan mengisi sesuai nilai yang sudah diapatkan.

```bash
log_path=metrics_agg_$(date +'%y%m%d%H').log
target_path=~

touch $log_path

echo type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size > $log_path
echo minimum,${log_min[0]},${log_min[1]},${log_min[2]},${log_min[3]},${log_min[4]},${log_min[5]},${log_min[6]},${log_min[7]},$target_path,${log_min[8]},${log_min[9]} >> $log_path
echo maximum,${log_max[0]},${log_max[1]},${log_max[2]},${log_max[3]},${log_max[4]},${log_max[5]},${log_max[6]},${log_max[7]},$target_path,${log_max[8]},${log_max[9]} >> $log_path
echo average,${log_avg[0]},${log_avg[1]},${log_avg[2]},${log_avg[3]},${log_avg[4]},${log_avg[5]},${log_avg[6]},${log_avg[7]},$target_path,${log_avg[8]},${log_avg[9]} >> $log_path
```

mengatur permission file log

```bash
chmod 700 $log_path
```

## Crontab

menjalankan script minute_log.sh setiap menit

```
* * * * * bash ~/SISOP/soal-shift-sisop-modul-1-f06-2022/soal3/minute_log.sh
```

menjalankan script aggregate_minutes_to_hourly_log.sh tiap jam (pada menit ke 0)

```
0 * * * * bash ~/SISOP/soal-shift-sisop-modul-1-f06-2022/soal3/aggregate_minutes_to_hourly_log.sh
```

## Screenshot dan Kendala

<img src=soal3/minute.png>
<img src=soal3/hourly.png>
<img src=soal3/crontabjalan.png>

Kendala yang dialami selama pengerjaan:

- tidak dapat agregasi disk usage karena dalam format human readable, kendala tersebut diselesaikan dengan mengkonversi disk usage menjadi format byte dulu sebelum diagregasi
- crontab tidak run script, solusinya adalah menggunakan path lengkap script pada crontab
