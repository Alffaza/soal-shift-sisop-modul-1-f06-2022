#!/bin/bash

#membuat folder baru
mkdir -p forensic_log_website_daffainfo_log

#rata-rata request per jam
awk '/22\/Jan\/2022/ {++total}
END {print "Rata-rata serangan adalah sebanyak ", total/24, "request per jam."}
' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt

#banyak serangan (ip)
ip_terbanyak_ans="$(awk 'BEGIN { FS=":" }
NR!=1 { arr[$1]++ }
END {
max=0
ip_max=0
for (a in arr){
	if(max < arr[a]){
		max=arr[a]
		ip_max=a
	}
}
print ip_max}
' log_website_daffainfo.log )"

#banyak serangan (ip)
banyak_serangan_ans="$(awk 'BEGIN { FS=":" }
NR!=1 { arr[$1]++ }
END {
max=0
ip_max=0
for (a in arr){
	if(max < arr[a]){
		max=arr[a]
	}
}
print max}
' log_website_daffainfo.log )"

#curl
curl_ans=$(awk '/curl/ {++n}
END {print n}' log_website_daffainfo.log )

#jam 2 pagi
awk -v curl_ans=$curl_ans -v ip_terbanyak_ans=$ip_terbanyak_ans -v banyak_serangan_ans=$banyak_serangan_ans '
BEGIN { FS=":"
print "IP yang paling banyak mengakses serveradalah:", ip_terbanyak_ans, "sebanyak", banyak_serangan_ans, "request\n"
printf("Ada %d request yang menggunakan curl sebagai user-agent\n\n", curl_ans)}
/22\/Jan\/2022:02/ {print $1}
' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt




