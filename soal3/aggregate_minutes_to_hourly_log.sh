#!/bin/bash

cd ~/log/
current_hour=$(date +'%H');
current_hour=$((current_hour - 1))
log_paths_substring=metrics_$(date +'%y%m%d')$current_hour
log_files=$(find -maxdepth 1 -name "$log_paths_substring*")  #find all files from the last hour

maxInt=1000000

log_totals=(0 0 0 0 0 0 0 0 0 0) #empty array
log_avg=(0 0 0 0 0 0 0 0 0 0)
log_min=($maxInt $maxInt $maxInt $maxInt $maxInt $maxInt $maxInt $maxInt $maxInt $maxInt)
log_max=(0 0 0 0 0 0 0 0 0 0)
total_logs=0

for log_file in $log_files
do
    total_logs=$((total_logs + 1))
    exec < $log_file
    read header
    read values
    IFS=,                       #change to accept commas as a seperator
    values_array=($values)
    values_array[9]=$(numfmt --from=iec ${values_array[10]}) #convert human readable to bytes
    for i in {0..9}
    do
        log_totals[$i]=$((log_totals[$i]+values_array[$i]))
        if [ $((values_array[$i])) -gt $((log_max[$i])) ]
        then
            log_max[$i]=$((values_array[$i]))
        fi
        if [ $((values_array[$i])) -lt $((log_min[$i])) ]
        then
            log_min[$i]=$((values_array[$i]))
        fi
    done
    IFS="\n"
done

j=0
for value in ${log_totals[@]}
do
    log_avg[$j]=$((value/total_logs))
    j=$((j+1))
done

log_min[9]=$(numfmt --to=iec ${log_min[9]}) #convert back into human readable format
log_max[9]=$(numfmt --to=iec ${log_max[9]})
log_avg[9]=$(numfmt --to=iec ${log_avg[9]})

log_path=metrics_agg_$(date +'%y%m%d%H').log
target_path=~

touch $log_path

echo type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size > $log_path
echo minimum,${log_min[0]},${log_min[1]},${log_min[2]},${log_min[3]},${log_min[4]},${log_min[5]},${log_min[6]},${log_min[7]},$target_path,${log_min[8]},${log_min[9]} >> $log_path
echo maximum,${log_max[0]},${log_max[1]},${log_max[2]},${log_max[3]},${log_max[4]},${log_max[5]},${log_max[6]},${log_max[7]},$target_path,${log_max[8]},${log_max[9]} >> $log_path
echo average,${log_avg[0]},${log_avg[1]},${log_avg[2]},${log_avg[3]},${log_avg[4]},${log_avg[5]},${log_avg[6]},${log_avg[7]},$target_path,${log_avg[8]},${log_avg[9]} >> $log_path

chmod 700 $log_path

