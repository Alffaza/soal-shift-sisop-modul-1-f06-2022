#!/bin/bash

update_log () {
    #get ram usage
    ram_usage=($(free -m))

    mem_total=${ram_usage[7]}
    mem_used=${ram_usage[8]}
    mem_free=${ram_usage[9]}
    mem_shared=${ram_usage[10]}
    mem_buff=${ram_usage[11]}
    mem_available=${ram_usage[12]}
    swap_total=${ram_usage[14]}
    swap_used=${ram_usage[15]}
    swap_free=${ram_usage[16]}

    #get disk usage
    target_path=~           #/home/usr
    disk_usage=($(du -sh $target_path))
    path_size=${disk_usage[0]}

    log_path=~/log/metrics_$(date +'%y%m%d%H%M%S').log
    touch $log_path
    echo -e "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,$target_path,$path_size" > $log_path
    chmod 700 $log_path
}

update_log